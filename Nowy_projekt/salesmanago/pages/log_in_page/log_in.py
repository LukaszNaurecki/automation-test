
"""Page object of the Log in page."""

from salesmanago.base.property_loader import PropertyLoaderClass
import urllib
import requests
import allure
import pytest
# from salesmanago.base.logging import logger
# from salesmanago.base.webdriver_custom_class import WebDriverCustomClass



class LogInPage(PropertyLoaderClass):

    def __init__(self, driver):
        self.driver = driver


    @allure.step("Login to account by cookie - user:'{1}' password:'{2}'")
    def login_by_cookie(self, user, password):
        user = urllib.parse.quote(user)
        session = requests.Session()

        self.driver.get(self.load_properties('url') + "/login.htm")
        session.post(self.load_properties('url') + '/j_spring_security_check?j_password=' + password +
                     '&j_username=' + user)
        r = session.get(self.load_properties('url') + "/app/home.htm")

        values_of_cookies = session.cookies.values()
        jsessionid = values_of_cookies[1]

        cookies = [dict(domain=self.load_properties('domain'), name=u'JSESSIONID', value=jsessionid, expiry=None,
                        path=u'/', httpOnly=False, secure=False)]
        self.driver.add_cookie(cookies[0])
        self.driver.get(self.load_properties('urlHome'))

        if "login.htm" not in r.url:
            with pytest.allure.step("Success logged into account"):
                pass
        else:
            raise Exception("Error with login to site")
