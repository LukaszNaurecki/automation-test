
"""Log in test - positive scenario."""

from salesmanago.base.base_setup import BaseSetup
from salesmanago.pages.home_page.home import HomePage
from salesmanago.pages.log_in_page.log_in import LogInPage
from salesmanago.base.property_loader import PropertyLoaderClass
import unittest


class LogInPositiveScenario(BaseSetup, PropertyLoaderClass, unittest.TestCase):

    def setUp(self):
        super(LogInPositiveScenario, self).setUp()
        self.home_page = HomePage(self.driver)
        self.log_in_page = LogInPage(self.driver)

    def test_log_in_into_account_by_cookie(self):
        self.log_in_page.login_by_cookie(self.load_properties('testUser'), self.load_properties('testUserPassword'))
        # self.home_page.verify_if_user_is_logged()

    def tearDown(self):
        super(LogInPositiveScenario, self).tearDown()

class LogInPositiveScenario1(BaseSetup, PropertyLoaderClass, unittest.TestCase):

    def setUp(self):
        super(LogInPositiveScenario1, self).setUp()
        self.home_page = HomePage(self.driver)
        self.log_in_page = LogInPage(self.driver)

    def test_log_in_into_account_by_cookie(self):
        self.log_in_page.login_by_cookie(self.load_properties('testUser'), self.load_properties('testUserPassword'))
        # self.home_page.verify_if_user_is_logged()

    def tearDown(self):
        super(LogInPositiveScenario1, self).tearDown()

if __name__ == '__main__':
    unittest.main()
