
"""Module to run all tests"""

import unittest
from salesmanago.tests.test_sign_in.test_sign_in_positive.sign_in_positive import LogInPositiveScenario


class TestSuiteRunner(object):

    def __init__(self):
        self.test_log_in_positive_scenario = unittest.TestLoader().loadTestsFromTestCase(LogInPositiveScenario)

    def run_all_tests(self):
        return unittest.TestSuite([self.test_log_in_positive_scenario])

if __name__ == '__main__':
    test_suite = TestSuiteRunner()
    unittest.TextTestRunner(verbosity=2).run(test_suite.run_all_tests())
