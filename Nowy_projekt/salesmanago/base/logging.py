import logging
import pytest, allure

def logs():
    logging.basicConfig(filename='logi.out', format='%(asctime)s - %(levelname)s', datefmt='%S', level=logging.INFO)

@allure.step('{0}')
def logger(message):
    logging.info(message)