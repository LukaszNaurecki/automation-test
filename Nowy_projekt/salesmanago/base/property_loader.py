import os
"""Properties loader - load configuration and properties"""
"""
    Read the file passed as parameter as a properties file.
"""


class PropertyPath(object):

    def set_path(self):
        dirname = os.path.dirname(__file__)
        # Here you can change property file
        filename = os.path.join(dirname, 'dev-salesmanago-properties')
        return filename


class PropertyLoaderClass(PropertyPath):

    def load_properties(self, name):
        props = {}
        sep = '='
        comment_char = '#'
        with open(self.set_path(), "r") as f:
            for line in f:
                l = line.strip()
                if l and not l.startswith(comment_char):
                    key_value = l.split(sep)
                    key = key_value[0].strip()
                    value = sep.join(key_value[1:]).strip().strip('"')
                    props[key] = value
        if name in props:
            return props[name]
        else:
            raise Exception("Invalid property key!")