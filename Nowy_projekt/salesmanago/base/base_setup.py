
"""Base setup - defined fixtures which are launched for every tests."""

from selenium import webdriver
from salesmanago.base.logging import logs
from salesmanago.base.property_loader import PropertyLoaderClass


class BaseSetup(PropertyLoaderClass):

    def setUp(self):
        if self.load_properties('browser') == "firefox":
            self.driver = webdriver.Firefox()
                # command_executor=self.load_properties('hubAddress'),
                # desired_capabilities={'browserName': 'firefox'})
        elif self.load_properties('browser') == 'chrome':
            self.driver = webdriver.Chrome()
                # command_executor=self.load_properties('hubAddress'),
                # desired_capabilities={'browserName': 'chrome'})
        else:
            raise Exception("Not supported browser!")
        self.driver.maximize_window()
        # logs()

    def tearDown(self):
        self.driver.quit()
